#ifndef _C_PREDATOR_
#define _C_PREDATOR_


#include "Predator.h"

namespace tld {


//Singleton of tld_predator
extern  Predator tld_pred;

//Sets the structural part of defaultParams
 void tld_config_structure(int frame_w, int frame_h, int n_ferns = N_FERNS,int n_features = N_FEATURES,float min_scale = MIN_SCALE, float max_scale = MAX_SCALE, int normalized_patch_dim = NORMALIZED_PATCH_DIM, int min_bb = MIN_BB, float min_window_scale = MIN_WINDOW_SCALE, float max_window_scale = MAX_WINDOW_SCALE, float base_window_scale = BASE_WINDOW_SCALE, float shift = SHIFT);

//Sets thresholds params
 void tld_config_thr(float decision_thr = DECISION_THRESHOLD , float min_positive_overlap = MIN_POSITIVE_OVERLAP, float max_negative_overlap = MAX_NEGATIVE_OVERLAP, float cutoff = CUTOFF, float theta_tp = THETA_TP, float theta_fp = THETA_FP);

 void tld_init(int frame_w, int frame_h);
 void tld_deinit();
 void tld_release();
 void tld_reset();

 void tld_select_object(Mat& img, BoundingBox& bb_in);
 void tld_process_frame(Mat& img, BoundingBox& bb_out);

}

#endif
