#ifndef _DEPTH_ESTIMATOR_
#define _DEPTH_ESTIMATOR_

#include <cmath>

const float PI = 3.141592653589793f;

static float defaultOffset[] = {0, 0/*0.4761f/2*/, 0};

float toRad(float angle);

float toDegree(float rad);	

float* estimate3DPos(int px, int py, float* pos3D, float nearPlan, int bbW, int bbH, float bbRealW = 0.40f/*0.15f*/, float bbRealH = 0.6f/*0.20f*/, int frameW = 640, int frameH = 480, float frameRealW = 0.4761f, float frameRealH = 0.275f, float* offset = defaultOffset/* Vector3 offset = Vector3(0, 0.4761f/2, 0)*/, float camFovH = toRad(57), float camFovV = toRad(43));

#endif