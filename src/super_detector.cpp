#include "human_detection/super_detector.h"



void SuperDetector::add_detector(CascadeDetector* detector)
{
	this->detectors.push_back(detector);
}

void SuperDetector::detect(Mat image)
{
	  detectedBB.valid = false;


	  for(int i = 0; i < detectors.size() && !detectedBB.valid; i++)
	  {
	  	detectors[i]->detect(image);

	  	if( detectors[i]->detectionResult.size() )
	  	{
	  		detectedBB = tld::BoundingBox(detectors[i]->detectionResult[0]);
	  		detectedBB.valid = true;
	  		detector_id = i;
	  	}
	  }
	 
}