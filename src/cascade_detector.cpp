
#include "human_detection/cascade_detector.h"






CascadeDetector::CascadeDetector( string filename )
{
        this->init(filename);
}

CascadeDetector::CascadeDetector( )
{
        this->isOk = false;
}

void CascadeDetector::init( string filename )
{
        this->isOk = detector_cascade.load(filename);
}

void CascadeDetector::detect( Mat& img )
{
        Mat frame_gray;

        cvtColor( img, frame_gray, CV_BGR2GRAY );
        equalizeHist( frame_gray, frame_gray );
        //-- Detect faces
        this->detectionResult.clear();
        detector_cascade.detectMultiScale( frame_gray, this->detectionResult, 1.15, 3, 0|CV_HAAR_SCALE_IMAGE, Size(60,60));//Size(22, 18));                
}