
#include "cpredator.h"

namespace tld
{


//Singleton of tld_predator
Predator tld_pred;

//Sets the structural part of tld_predator. It's necessary to reset and recreate internal structures.
void tld_config_structure(int frame_w, int frame_h,int n_ferns,int n_features,float min_scale, float max_scale, int normalized_patch_dim, int min_bb, float min_window_scale, float max_window_scale, float base_window_scale, float shift)
{
    tld_init(frame_w,frame_h);

    tld_pred.params->frame_w = frame_w;
    tld_pred.params->frame_h = frame_h;

    tld_pred.params->n_ferns = n_ferns;
    tld_pred.params->n_features = n_features;
    tld_pred.params->min_scale = min_scale;
    tld_pred.params->max_scale = max_scale;
    tld_pred.params->normalized_patch_dim = normalized_patch_dim;
    tld_pred.params->min_bb = min_bb;
    tld_pred.params->min_window_scale = min_window_scale;
    tld_pred.params->max_window_scale = max_window_scale;
    tld_pred.params->base_window_scale = base_window_scale;
    tld_pred.params->shift = shift;

    if( !tld_pred.initialized ) {


        tld_pred.init();

    }
    else
    {
        tld_pred.reset();
    }
}

//Sets thresholds params. No need to reset tld_predator. It's up to the programmer to decide it.
void tld_config_thr(float decision_thr, float min_positive_overlap, float max_negative_overlap, float cutoff, float theta_tp, float theta_fp)
{
    tld_pred.params->decision_threshold = decision_thr;
    tld_pred.params->min_positive_overlap = min_positive_overlap;
    tld_pred.params->max_negative_overlap = max_negative_overlap;
    tld_pred.params->cutoff = cutoff;
    tld_pred.params->cutoff = cutoff;
    tld_pred.params->theta_tp = theta_tp;
    tld_pred.params->theta_fp = theta_fp;
}

//If the singleton wasn't created, it is created when this procedure is called.
void tld_init(int frame_w, int frame_h)
{	
	/*
    if( !tld_pred )
    {
        tld_pred = Predator(frame_w,frame_h);
    }
	*/
}

//Clear tld_predator singleton object, freeing memory
void tld_deinit()
{
    //delete tld_pred;
    //tld_pred = NULL;
}

//Reset tracking, deletes online model, reinitialize everything according to current parameters
void tld_reset()
{
    tld_pred.reset();
}

//Selects the first example. This will be the object that will be tracked by the TLD/Predator Algorithm
void tld_select_object(Mat& img, BoundingBox& bb_in)
{
    tld_pred.selectObject(img, bb_in);
}


//Process the given frame (img) and if it was possible returns the tracked object position on the same frame (bb_out).
void tld_process_frame(Mat& img, BoundingBox& bb_out)
{
    tld_pred.processFrame(img);
    bb_out = tld_pred.currBB;
}

}
