#include "human_detection/depth_estimator.h"

float toRad(float angle){
	return (angle*PI)/180.0f;
}

float toDegree(float rad)
{
	return (rad*180.0f)/PI;
}	

float* estimate3DPos(int px, int py, float* pos3D, float nearPlan, int bbW, int bbH, float bbRealW, float bbRealH, int frameW, int frameH, float frameRealW, float frameRealH, float* offset, float camFovH, float camFovV)
{
	float rppH = camFovH/frameW;
	float rppV = camFovV/frameH;

	float alpha = (rppH*bbW)/2;
	float beta = (rppV*bbH)/2;
	float zAlpha = bbRealW/(2.0f*tanf(alpha));
	float zBeta = bbRealH/(2.0f*tanf(beta));

	//from pixel world to real world metric system
	float transformed_px = (float(frameW - 2*px)/frameW)*frameRealW;
	float transformed_py = (float(frameH - 2*py)/frameH)*frameRealH;

	pos3D[0] = (transformed_px*zAlpha) + offset[0];
	pos3D[1] = (transformed_py*zBeta) + offset[1];
	pos3D[2] = (zAlpha+zBeta)/2.0f + offset[2];

	return pos3D;	
}
